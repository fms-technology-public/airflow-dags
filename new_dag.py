from airflow import DAG
from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import KubernetesPodOperator
from airflow.utils.dates import days_ago
from datetime import timedelta
from kubernetes.client import models as k8s

# Default arguments for the DAG
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

# Define the DAG
dag = DAG(
    'new_processor_kubernetes',
    default_args=default_args,
    description='Processor for STR genomic data',
    schedule_interval=timedelta(days=1),
    start_date=days_ago(1),
    tags=['example'],
)

# Define the task using KubernetesPodOperator
new_task = KubernetesPodOperator(
    image="registry.vissoft.vn/vissoft/mash:0.1",
    image_pull_secrets=[k8s.V1LocalObjectReference("visregistry")],
    cmds=["python3", "-u"],
    arguments=["run_parser.py","arg1","arg2","arg3"],
    labels={"foo": "bar"},
    name="new-task",
    task_id="new_task",
    get_logs=True,
    is_delete_operator_pod=True,  # Ensure the pod gets cleaned up after execution
    in_cluster=True,
    dag=dag,
)
