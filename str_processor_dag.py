from airflow import DAG
from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import KubernetesPodOperator
from airflow.utils.dates import days_ago
from datetime import timedelta
from kubernetes.client import models as k8s

# Các tham số mặc định
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    # Gửi email khi có lỗi & retry cũng không thành công
    'email_on_failure': False,
    # Gửi email khi thực hiện retry
    'email_on_retry': False,
    # Số lần retry
    'retries': 1,
    # Thời gian chờ trước khi retry
    'retry_delay': timedelta(minutes=5),
}

# Cấu hình DAG
dag = DAG(
    'str_processor_kubernetes',
    default_args=default_args,
    description='Processor for STR genomic data',
    # Tần suất chạy
    schedule_interval=timedelta(days=1),
    # Thời điểm bắt đầu
    start_date=days_ago(1),
    # Gắn thẻ
    tags=['example'],
)

# Cấu hình KubernetesPodOperator
task = KubernetesPodOperator(
    image="registry.vissoft.vn/vissoft/mash:0.6",
    image_pull_secrets=[k8s.V1LocalObjectReference("visregistry")],
    cmds=["python3", "-u"],
    # Lệnh chạy script & các tham số
    arguments=["run_parser.py"],
    # Dán nhãn cho worker container
    labels={"foo": "bar"},
    # Tên task pod
    name="str-processor-task",
    # ID của task
    task_id="str_processor_task",
    env_vars={
        # "BOOTSTRAP_SERVERS": "$(bootstrap_servers)",
        # "SECURITY_PROTOCOL": "$(security_protocol)",
        # "SASL_USERNAME": "$(sasl_username)",
        # "SASL_PASSWORD": "$(sasl_password)",
        # "CLIENT_NAME": "$(client_name)",
        # "GROUP_ID": "$(group_id)",
        # "MQ_QUEUE_NAME": "$(mq_queue_name)",
        # "MQ_DESTINATION": "$(mq_destination)",
        # "THREADS_NUM": "$(threads_num)",
        # "MINIO_URL": "$(minio_url)",
        # "MINIO_ACCESS_KEY": "$(minio_access_key)",
        # "MINIO_SECRET_KEY": "$(minio_secret_key)",
        # "BOTO_ACCESS_KEY": "$(boto_access_key)",
        # "BOTO_SECRET": "$(boto_secret)",
        # "BOTO_ENDPOINT": "$(boto_endpoint)"
    },
    configmaps=['mash-config'],
    get_logs=True,
    # Xóa pod sau khi chạy thành công
    is_delete_operator_pod=True,
    in_cluster=True,
    dag=dag,
)
